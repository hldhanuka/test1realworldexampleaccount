// class represents user-defined exception  
class ExceptionHandling6UserDefinedException extends Exception {
    public ExceptionHandling6UserDefinedException(String str) {
        // Calling constructor of parent Exception
        super(str);
    }
}

// Class that uses above MyException  
public class ExceptionHandling6 {
    public static void main(String args[]) {
        try {
            // throw an object of user defined exception
            throw new ExceptionHandling6UserDefinedException("This is user-defined exception");
        } catch (ExceptionHandling6UserDefinedException ude) {
            System.out.println("Caught the exception");
            // Print the message from MyException object
            System.out.println(ude.getMessage());
        }
    }
}
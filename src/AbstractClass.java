abstract class AbstractClassBike {
    abstract void run();
}

class AbstractClass extends AbstractClassBike {
    void run() {
        System.out.println("running safely");
    }

    public static void main(String args[]) {
        AbstractClassBike obj = new AbstractClass();
        obj.run();
    }
}
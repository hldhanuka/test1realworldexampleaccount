import java.io.*;

public class JavaByteArrayOutputStream {
    public static void main(String args[]) throws Exception {
        FileOutputStream fout1 = new FileOutputStream("JavaByteArrayOutputStreamf1.txt");
        FileOutputStream fout2 = new FileOutputStream("JavaByteArrayOutputStreamf2.txt");

        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(65);
        bout.writeTo(fout1);
        bout.writeTo(fout2);

        bout.flush();
        bout.close();//has no effect
        System.out.println("Success...");
    }
}
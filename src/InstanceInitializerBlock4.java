class InstanceInitializerBlock4A {
    InstanceInitializerBlock4A() {
        System.out.println("parent class constructor invoked");
    }
}

class InstanceInitializerBlock4 extends InstanceInitializerBlock4A {
    InstanceInitializerBlock4() {
        super();
        System.out.println("child class constructor invoked");
    }

    InstanceInitializerBlock4(int a) {
        super();
        System.out.println("child class constructor invoked " + a);
    }

    {
        System.out.println("instance initializer block is invoked");
    }

    public static void main(String args[]) {
        InstanceInitializerBlock4 b1 = new InstanceInitializerBlock4();
        InstanceInitializerBlock4 b2 = new InstanceInitializerBlock4(10);
    }
}
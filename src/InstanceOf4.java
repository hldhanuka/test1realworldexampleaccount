class InstanceOf4Animal {
}

class InstanceOf4 extends InstanceOf4Animal {
    static void method(InstanceOf4Animal a) {
        if (a instanceof InstanceOf4) {
            InstanceOf4 d = (InstanceOf4) a;//downcasting
            System.out.println("ok downcasting performed");
        }
    }

    public static void main(String[] args) {
        InstanceOf4Animal a = new InstanceOf4();
        InstanceOf4.method(a);
    }

}
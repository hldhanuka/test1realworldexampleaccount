class InstanceOf2Animal {
}

class InstanceOf2 extends InstanceOf2Animal {//Dog inherits Animal

    public static void main(String args[]) {
        InstanceOf2 d = new InstanceOf2();
        System.out.println(d instanceof InstanceOf2Animal);//true
    }
}
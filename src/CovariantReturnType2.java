class AA1 {
    AA1 foo() {
        return this;
    }

    void print() {
        System.out.println("Inside the class A1");
    }
}


// A2 is the child class of A1  
class AA2 extends AA1 {
    @Override
    AA1 foo() {
        return this;
    }

    void print() {
        System.out.println("Inside the class A2");
    }
}

// A3 is the child class of A2  
class AA3 extends AA2 {
    @Override
    AA1 foo() {
        return this;
    }

    @Override
    void print() {
        System.out.println("Inside the class A3");
    }
}

public class CovariantReturnType2 {
    // main method
    public static void main(String argvs[]) {
        AA1 a1 = new AA1();

        // this is ok
        a1.foo().print();

        AA2 a2 = new AA2();

        // we need to do the type casting to make it
        // more clear to reader about the kind of object created
        ((AA2) a2.foo()).print();

        AA3 a3 = new AA3();

        // doing the type casting
        ((AA3) a3.foo()).print();

    }
}
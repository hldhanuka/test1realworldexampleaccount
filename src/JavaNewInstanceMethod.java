class JavaNewInstanceMethodSimple {
    void message() {
        System.out.println("Hello Java");
    }
}

class JavaNewInstanceMethod {
    public static void main(String args[]) {
        try {
            Class c = Class.forName("Simple");
            JavaNewInstanceMethodSimple s = (JavaNewInstanceMethodSimple) c.newInstance();
            s.message();

        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
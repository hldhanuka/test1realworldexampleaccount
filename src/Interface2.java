//Interface declaration: by first user  
interface Interface2Drawable {
    void draw();
}

//Implementation: by second user  
class Interface2Rectangle implements Interface2Drawable {
    public void draw() {
        System.out.println("drawing rectangle");
    }
}

class Interface2Circle implements Interface2Drawable {
    public void draw() {
        System.out.println("drawing circle");
    }
}

//Using interface: by third user  
class Interface2 {
    public static void main(String args[]) {
        Interface2Drawable d = new Interface2Circle();//In real scenario, object is provided by method e.g. getDrawable()
        d.draw();
    }
}
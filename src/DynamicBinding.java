class DynamicBindingAnimal {
    void eat() {
        System.out.println("animal is eating...");
    }
}

class DynamicBinding extends DynamicBindingAnimal {
    void eat() {
        System.out.println("dog is eating...");
    }

    public static void main(String args[]) {
        DynamicBindingAnimal a = new DynamicBinding();
        a.eat();
    }
}
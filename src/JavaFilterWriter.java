import java.io.*;

class JavaFilterWriterCustomFilterWriter extends FilterWriter {
    JavaFilterWriterCustomFilterWriter(Writer out) {
        super(out);
    }

    public void write(String str) throws IOException {
        super.write(str.toLowerCase());
    }
}

public class JavaFilterWriter {
    public static void main(String[] args) {
        try {
            FileWriter fw = new FileWriter("JavaFilterWriterRecord.txt");
            JavaFilterWriterCustomFilterWriter filterWriter = new JavaFilterWriterCustomFilterWriter(fw);
            filterWriter.write("I LOVE MY COUNTRY");
            filterWriter.close();
            FileReader fr = new FileReader("JavaFilterWriterrecord.txt");
            BufferedReader bufferedReader = new BufferedReader(fr);
            int k;
            while ((k = bufferedReader.read()) != -1) {
                System.out.print((char) k);
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
class InstanceOf5Animal {
}

class InstanceOf5 extends InstanceOf5Animal {
    static void method(InstanceOf5Animal a) {
        InstanceOf5 d = (InstanceOf5) a;//downcasting
        System.out.println("ok downcasting performed");
    }

    public static void main(String[] args) {
        InstanceOf5Animal a = new InstanceOf5();
        InstanceOf5.method(a);
    }
}
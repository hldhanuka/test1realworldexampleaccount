class InstanceInitializerBlock3A {
    InstanceInitializerBlock3A() {
        System.out.println("parent class constructor invoked");
    }
}

class InstanceInitializerBlock3 extends InstanceInitializerBlock3A {
    InstanceInitializerBlock3() {
        super();
        System.out.println("child class constructor invoked");
    }

    {
        System.out.println("instance initializer block is invoked");
    }

    public static void main(String args[]) {
        InstanceInitializerBlock3 b = new InstanceInitializerBlock3();
    }
}
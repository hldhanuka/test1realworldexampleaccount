//Java Program to demonstrate why we need method overriding  
//Here, we are calling the method of parent class with child  
//class object.  
//Creating a parent class  
class Vehicle {
    void run() {
        System.out.println("Vehicle is running");
    }
}

//Creating a child class  
class MethodOverriding extends Vehicle {
    public static void main(String args[]) {
        //creating an instance of child class
        MethodOverriding obj = new MethodOverriding();
        //calling the method with child class instance
        obj.run();
    }
}
abstract class AbstractClass2Shape {
    abstract void draw();
}

//In real scenario, implementation is provided by others i.e. unknown by end user  
class AbstractClass2Rectangle extends AbstractClass2Shape {
    void draw() {
        System.out.println("drawing rectangle");
    }
}

class AbstractClass2Circle1 extends AbstractClass2Shape {
    void draw() {
        System.out.println("drawing circle");
    }
}

//In real scenario, method is called by programmer or user  
class AbstractClass2 {
    public static void main(String args[]) {
        AbstractClass2Shape s = new AbstractClass2Circle1();//In a real scenario, object is provided through method, e.g., getShape() method
        s.draw();
    }
}
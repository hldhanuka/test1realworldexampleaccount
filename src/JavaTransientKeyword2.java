import java.io.*;

class JavaTransientKeyword2 {
    public static void main(String args[]) throws Exception {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("JavaTransientKeywordf.txt"));
        JavaTransientKeywordStudent s = (JavaTransientKeywordStudent) in.readObject();
        System.out.println(s.id + " " + s.name + " " + s.age);
        in.close();
    }
}
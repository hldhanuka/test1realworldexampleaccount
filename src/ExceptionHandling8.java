import java.io.*;

class ExceptionHandling8M {
    void method() throws IOException {
        throw new IOException("device error");
    }
}

public class ExceptionHandling8 {
    public static void main(String args[]) {
        try {
            ExceptionHandling8M m = new ExceptionHandling8M();
            m.method();
        } catch (Exception e) {
            System.out.println("exception handled");
        }

        System.out.println("normal flow...");
    }
}
class NestedInterface2A {
    interface Message {
        void msg();
    }
}

class NestedInterface2 implements NestedInterface2A.Message {
    public void msg() {
        System.out.println("Hello nested interface");
    }

    public static void main(String args[]) {
        NestedInterface2A.Message message = new NestedInterface2();//upcasting here
        message.msg();
    }
}
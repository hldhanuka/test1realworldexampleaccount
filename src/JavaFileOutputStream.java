import java.io.FileOutputStream;

public class JavaFileOutputStream {
    public static void main(String args[]) {
        try {
            FileOutputStream fout = new FileOutputStream("JavaFileOutputStreamTestout.txt");
            fout.write(65);
            fout.close();
            System.out.println("success...");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
import java.io.*;

class ExceptionHandlingMethodOverriding2Parent {
    void msg() throws Exception {
        System.out.println("parent method");
    }
}

public class ExceptionHandlingMethodOverriding2 extends ExceptionHandlingMethodOverriding2Parent {
    void msg() throws Exception {
        System.out.println("child method");
    }

    public static void main(String args[]) {
        ExceptionHandlingMethodOverriding2Parent p = new ExceptionHandlingMethodOverriding2();

        try {
            p.msg();
        } catch (Exception e) {
        }
    }
}
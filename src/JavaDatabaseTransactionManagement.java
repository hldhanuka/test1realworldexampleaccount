import java.sql.*;

class JavaDatabaseTransactionManagement {
    public static void main(String args[]) throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/java-basic-project-1", "root", "");
        con.setAutoCommit(false);

        Statement stmt = con.createStatement();
        stmt.executeUpdate("insert into emp values(190,'abhi',40000)");
        stmt.executeUpdate("insert into emp values(191,'umesh',50000)");

        con.commit();
        con.close();
    }
}
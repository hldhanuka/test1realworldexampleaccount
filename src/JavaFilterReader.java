import java.io.*;

class JavaFilterReaderCustomFilterReader extends FilterReader {
    JavaFilterReaderCustomFilterReader(Reader in) {
        super(in);
    }

    public int read() throws IOException {
        int x = super.read();
        if ((char) x == ' ')
            return ((int) '?');
        else
            return x;
    }
}

public class JavaFilterReader {
    public static void main(String[] args) {
        try {
            Reader reader = new FileReader("JavaFilterReaderjavaFile123.txt");
            JavaFilterReaderCustomFilterReader fr = new JavaFilterReaderCustomFilterReader(reader);
            int i;
            while ((i = fr.read()) != -1) {
                System.out.print((char) i);
            }
            fr.close();
            reader.close();
        } catch (Exception e) {
            e.getMessage();
        }
    }
}
class RuntimePolymorphism2Bank {
    float getRateOfInterest() {
        return 0;
    }
}

class RuntimePolymorphism2SBI extends RuntimePolymorphism2Bank {
    float getRateOfInterest() {
        return 8.4f;
    }
}

class RuntimePolymorphism2ICICI extends RuntimePolymorphism2Bank {
    float getRateOfInterest() {
        return 7.3f;
    }
}

class RuntimePolymorphism2AXIS extends RuntimePolymorphism2Bank {
    float getRateOfInterest() {
        return 9.7f;
    }
}

class RuntimePolymorphism2 {
    public static void main(String args[]) {
        RuntimePolymorphism2Bank b;
        b = new RuntimePolymorphism2SBI();
        System.out.println("SBI Rate of Interest: " + b.getRateOfInterest());
        b = new RuntimePolymorphism2ICICI();
        System.out.println("ICICI Rate of Interest: " + b.getRateOfInterest());
        b = new RuntimePolymorphism2AXIS();
        System.out.println("AXIS Rate of Interest: " + b.getRateOfInterest());
    }
}
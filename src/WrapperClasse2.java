//Creating the custom wrapper class  
class WrapperClasse2Javatpoint {
    private int i;

    WrapperClasse2Javatpoint() {
    }

    WrapperClasse2Javatpoint(int i) {
        this.i = i;
    }

    public int getValue() {
        return i;
    }

    public void setValue(int i) {
        this.i = i;
    }

    @Override
    public String toString() {
        return Integer.toString(i);
    }
}

//Testing the custom wrapper class  
public class WrapperClasse2 {
    public static void main(String[] args) {
        WrapperClasse2Javatpoint j = new WrapperClasse2Javatpoint(10);
        System.out.println(j);
    }
}
class FinalVariable {
    final int speedlimit;//blank final variable

    FinalVariable() {
        speedlimit = 70;
        System.out.println(speedlimit);
    }

    public static void main(String args[]) {
        new FinalVariable();
    }
}
interface Interface6Drawable {
    void draw();

    static int cube(int x) {
        return x * x * x;
    }
}

class Interface6Rectangle implements Interface6Drawable {
    public void draw() {
        System.out.println("drawing rectangle");
    }
}

class Interface6 {
    public static void main(String args[]) {
        Interface6Drawable d = new Interface6Rectangle();
        d.draw();
        System.out.println(Interface6Drawable.cube(3));
    }
}
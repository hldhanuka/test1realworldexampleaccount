import java.io.*;

class JavaDeserialization {
    public static void main(String args[]) {
        try {
            //Creating stream to read the object
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("JavaDeserializationf.txt"));
            Student s = (Student) in.readObject();
            //printing the data of the serialized object
            System.out.println(s.name + " " + s.name);
            //closing the stream
            in.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
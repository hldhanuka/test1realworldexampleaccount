import java.io.*;

public class JavaDataOutputStream {
    public static void main(String[] args) throws IOException {
        FileOutputStream file = new FileOutputStream("JavaDataOutputStreamtestout.txt");
        DataOutputStream data = new DataOutputStream(file);
        data.writeInt(65);
        data.flush();
        data.close();
        System.out.println("Succcess...");
    }
}
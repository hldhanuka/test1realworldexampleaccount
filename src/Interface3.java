interface Interface3Printable {
    void print();
}

interface Interface3Showable {
    void show();
}

class Interface3 implements Interface3Printable, Interface3Showable {
    public void print() {
        System.out.println("Hello");
    }

    public void show() {
        System.out.println("Welcome");
    }

    public static void main(String args[]) {
        Interface3 obj = new Interface3();
        obj.print();
        obj.show();
    }
}